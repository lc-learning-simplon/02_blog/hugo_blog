---
title: "Charte Graphique"
date: 2019-07-10T10:15:48+02:00
draft: false
description: "Veille sur la charte graphique"
img: ""
author: "Lucas et Cathy"
---

## Pour une bonne Charte Graphique

### Etape 1: Choisissez la palette de couleurs adaptés à votre domaine.


<img src="/img/chartegraph/definiton.png" alt="" style="width: 30%;">


Il est important de prendre en compte la perception naturelle et le message véhiculé à travers les couleurs pour choisir la palette qui correspond le mieux à votre domaine d'activité, mais aussi aux valeurs que vous voulez transmettre. La plupart des marques choisissent 4 couleurs maximum qui se rapprochent de celle de leur logo.

– Le rouge est la couleur de prédilection du secteur alimentaire car elle stimule la faim mais aussi de la grande distribution et des médias car elle a un fort impact sur l’attention. 

– Le orange est la couleur du divertissement car elle véhicule la sociabilité et l’accessibilité, c’est aussi une couleur phare pour le domaine de la mobilité comme le logo EasyJet ou encore pour celui du sport car le orange est énergique.

– Le jaune est la couleur favorite du secteur de l’énergie et du transport. C’est une couleur optimiste, créative et visionnaire.

– Le vert est la couleur des acteurs de l’écologie et de la santé. En effet, le vert véhicule un sentiment de bien-être et est souvent associé à des enseignes écologiques.

– Le bleu est particulièrement adapté pour les entreprises de voyage, les banques, les assurances et le secteur high-tech. C’est une couleur qui rassure, qui évoque la fiabilité et qui est ingénieuse.

– Le violet est la couleur de prédilection du secteur scolaire et de tout ce qui touche à l’art et la culture. C’est la couleur préférée des rêveurs !

– Le rose est souvent utilisé dans le secteur de l’enfance, de la cosmétique et de la pâtisserie. C’est une couleur espiègle qui évoque aussi la féminité et la douceur.

– Le gris est la couleur de la finance, de l’automobile et de la technologie. Classique, le gris véhicule la neutralité et la persévérance.

– Le noir est beaucoup utilisé dans le luxe, les arts et le high-tech. C’est une couleur à la fois élégante, mystérieuse et déterminée.

### Etape 2: Police de caractère.


<img src="/img/chartegraph/police.jpg" alt="" style="width: 30%;">


En ce qui concerne le choix de vos polices de caractères, vous devez prendre en compte votre domaine d'activité ainsi que votre cible, pour utiliser une typographie adaptée. Cela va de soi, un boucher ne communiquera pas avec la même police qu'une startup high-tech. Si vous travaillez sur un marché "traditionnel" comme le consulting, l'immobiliter, le juridique... mieux vaut privilégier les polices classiques. En revanche, si votre entreprise est sur un domaine plus artistique, vous pouvez oser les jeux typographiques. N'hésitez pas non plus à vous inspirer des tendances du moment des typographies. 

### Etape 3: Définissez la place du logo et les règles d'utilisation de celui-ci.


<img src="/img/chartegraph/logooui.png" alt="" style="width: 40%;">
<img src="/img/chartegraph/logonon.png" alt="" style="width: 40%;">


Il vous faut maintenant penser à la manière dont votre logo s'intégrera aux différents supports que vous allez utiliser. Lors de cette étape, vous devez donc expliquer les façons de représenter votre logo en fonction des différents environnements auxquels il est destiné. Cela permet notamement d'éviter que votre logo soit étiré, compressé, modifié ou mal aligné et que votre image de marque en prenne un coup. 


#### Pour cela, plusieurs aspects sont à déterminer:

##### Les dimensions:

    Quelles sont les dimensions minimales de votre logo? Quelles proportions respecter?


##### L'Espace:

    Dans le cas où votre logo requiert une certaine quantité d'espace vide (autour par exemple) pour être lisible, il vous faudra rédiger vos instructions. Par exemple, lorsque votre logo est utilisé à côté d'autres éléments, le fait de prévoir une marge permet une meilleure lisibilité de celui-ci. 


##### La Couleur:

    Quel est le code couleur à utiliser? Quelles sont les différentes utilisations possibles des couleurs? Quelles variations sont possibles (noir et blanc, 1 seule couleur, etc...)? En effet, en fonction de la couleur du fond, le logo ressortira plus ou moins bien. 


##### A proscrire:

    Il vous faudra illustrer les choses à ne pas faire et au contraire, les bonnes pratiques, pour guider les différents acteurs dans l'utilisation de votre logo. 


### Etape 4: Identifiez l'univers visuel qui correspond à votre manque. 


<img src="/img/chartegraph/identifier.png" alt="" style="width: 30%;">


Cette étape consiste à expliquer les différentes façon dont vous souhaitez que l'on communique sur votre entreprise. Pour cela, identifier le style d'images, les illustrations et les pictos qui correspondent le mieux à votre entreprise. Cela vous permettra dans un premier temps d'avoir un univers visuel cohérent. Créer une charte graphique est très bénéfique pour toutes les autres personnes impliquées dans votre entreprise qui sauront comment utiliser les images pour illustrer vos supports de communication. Vous pouvez aussi intégrer des exemples d'images qui ont bien fonctionné pour votre entreprise ou fournir des exemples inspirants. Le moodboard est un outil souvent utilisé pour créer une charte graphique et rassembler des images qui évoquent les messages et les émotions que vous souhaitez véhiculer. 

### Etape 5: Déclinez votre identité visuelle sur les différents supports.


<img src="/img/chartegraph/pub.jpg" alt="" style="width: 30%;">


En déclinant votre identité visuelle sur l'ensemble de vos outils de communication, vous gagnerez en crédibilité et en notoriété. L'idée d'une charte graphique est donc de pouvoir définir et d'adapter plus facilement votre univers visuel sur tous vos supports. Ainsi, vous pourrez communiquer aisément avec la même typographie, les mêmes couleurs et la même mise en page pour bâtir une communication cohérente et harmonieuse. 

## Liens:

https://www.mantalo-conseil.fr/creer-votre-univers-graphique

https://www.blogdigital.fr/identite-numerique/#.XSTgQCY6_M2

https://www.creads.fr/blog/comment-faire/creer-une-charte-graphique

https://cocoricoweb.com/comment-faire-une-charte-graphique/


Merci à vous. 