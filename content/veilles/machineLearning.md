---
title: "Machine Learning"
date: 2019-08-13T16:40:19+02:00
draft: false
description: "Veille sur le Machine Learning."
img: "/img/machine-learning/deepLearning.gif"
author: "Cathy"
---
## Description

Le « machine learning » est l’apprentissage automatique à partir des données sans programmation explicite.Cela veut dire, qu’au lieu de trouver des règles pour automatiser une tâche, on transmet des données à un algorithme qui va définir ces règles lui-même. Ces données sont représentatives du problème qu’on veut résoudre. 
Plus il y a de données, plus il est précis.
Dans le domaine de l’informatique, le machine learning est vu comme de l’intelligence artificielle pilotée par les données. Ainsi, l’IA n’est pas forcément que du machine learning, bien que, ce dernier peut la rendre plus pratique. Comme dans le cas de la compréhension des langues ou la reconnaissance des objets. Il y a plusieurs catégories de Machine Learning, apprentissage données classique et simple et l'apprentissage par "couche" (plus en profondeur).

## Le Processus d'apprentissage

Le processus d’apprentissage se fait en trois phases principales : la préparation des données, la construction du modèle et la phase d’évaluation et validation. Ce processus est illustré par le schéma suivant :

<img src="/img/machine-learning/process.png" alt="" style="width: 40%;">

#### Préparation des données 

Cette première étape consiste à :
    • Collecter des données qui peuvent être de différentes sources (entrepôts de données, web, capteurs, etc…) et de différents types (numériques, textuelles, images, SQL, graphes, séquences, etc…). 
    • Nettoyer, préparer et normaliser les données pour pouvoir les transmettre aux algorithmes d’apprentissage.

#### Construction du modèle

Cette étape consiste à :
    • Choisir l’algorithme le plus adapté à la problématique et à la nature des données d’apprentissage.
    • Effectuer l’apprentissage en fournissant les données préparées à l'algorithme pour construire un modèle.
    • Évaluer et améliorer le modèle selon les résultats obtenus avec les données de test.

#### Évaluation et validation

Cette étape consiste à :
    • Observer les performances du modèle sur de nouvelles données afin de voir s'il effectue les prédictions ou les classements selon les résultats souhaités.
    • Parfois, demander l’avis d’un expert métier pour valider le modèle ou sinon voir les points qui n’ont pas été considérés.
    • Intégrer la solution, faire des prédictions, publier la solution, créer des stratégies business.

Remarquez qu’on peut revenir en arrière, ajouter de nouveaux jeux de données et refaire de nouvelles expérimentations, afin de comparer les résultats obtenus avec ceux des expériences effectuées auparavant jusqu'à obtention d'un résultat satisfaisant.

## Exemple

### Linéaire, non linéaire

<img src="/img/machine-learning/iris.png" alt="" style="width: 30%;">

Les algorithmes de classification linéaire, assument que les classes peuvent être séparées par un hyperplan pour répondre à une problématique. Dans le cas des données non- séparables linéairement, les algorithmes se basent sur des fonctions d’activation non-linéaires. 

Dans l’exemple suivant on a trois types de fleurs (Setosa, Virginica et Versicolor)représentées par la longueur et la largeur de leurs pétales et sépales. Les fleurs Setosa et Versicolor sont très bien séparées linéairement. Les deux types Versicolor et Virginica partagent quelques caractéristiques alors qu’elles sont de catégories différentes. Donc si on utilise un algorithme linéaire pour faire la classification de ces données, celui-ci sépare les données seulement en deux classes et considère les deux types Versicolor et Virginica comme s’ils sont du même type alors que ce n’est pas le cas. Néanmoins, un algorithme non-linéaire permet de trouver une séparation des trois types de fleurs dans une projection non-linéaire.

### Paramétrique, non-paramétrique

<img src="/img/machine-learning/parametrique.png" alt="" style="width: 40%;">

Les algorithmes paramétriques nécessitent la configuration préalable d’un nombre fixe de paramètres, le choix de ces paramètres se fait selon une ou plusieurs hypothèses que l'on formule sur la distribution des données. Le résultat obtenu dépendra des paramètres choisis. 
Les algorithmes non-paramétriques ont leurs propres fonctions qui définissent de manière adaptative les paramètres en fonction de la taille, la nature des jeux de données ainsi que les conditions d’apprentissage. De cette façon ils sont capables de faire correspondre convenablement la représentation de l’entrée à celle de la sortie.

### Incrémental, hors-ligne

Dans le cas de la reconnaissance des langues, un modèle hors-ligne qui a déjà appris à reconnaître le français et l’anglais mais qui ne comprend pas l’espagnol, ne peut pas apprendre l’espagnol séparément. Il lui faut un corpus de la langue espagnole combiné avec les corpus français et anglais pour qu’il apprenne à nouveau les trois langues. Un modèle incrémental quant à lui, enregistre les caractéristiques de la nouvelle langue détectée et les utilise lors de sa prochaine tentative d’identification.


### Probabiliste, Géométrique

<img src="/img/machine-learning/incrémental.png" alt="" style="width: 40%;">

Les algorithmes probabilistes permettent d’avoir la distribution des probabilités d’appartenance d’un exemple d’entrée à chaque classe. Alors que les algorithmes géométriques ne modélisent pas la distribution des classes, mais séparent l'espace par des fonctions et renvoient la classe associée à l'espace d'où provient un échantillon de données.


### Algorithme avec apprentissage supervisé

Ce type d’algorithme reçoit comme données d’apprentissage un ensemble d’entrées avec leurs équivalents connus de sorties. C’est à eux d’optimiser la relation « entrée -> sortie » et de la généraliser à des entrées inconnues.
On peut distinguer deux catégories différentes de l’apprentissage supervisé :

#### Classification

<img src="/img/machine-learning/comments.png" alt="" style="width: 40%;">

Dans ce cas, le principe est d’attribuer une classe d’appartenance à chaque échantillon de données inconnu. 
Par exemple, dans le cadre de l’analyse des sentiments dans des commentaires, l’algorithme reçoit des commentaires en entrée, classifiés par avance en positif ou négatif, afin qu’il apprenne lui-même à prédire la classe d’un nouveau commentaire.

#### Régression :

<img src="/img/machine-learning/sales.png" alt="" style="width: 40%;">

Dans ce cas, l’algorithme ne prédit pas une classe d’appartenance mais une valeur continue.
Comme par exemple le cas de la prédiction des ventes, l’algorithme prédit le nombre approximatif des unités qui seront vendues par jour.


### Algorithme avec apprentissage non-supervisé

<img src="/img/machine-learning/clustering-iris-data.gif" alt="" style="width: 40%;">

L’apprentissage non supervisé, permet de découvrir les patterns pertinents et les tendances complexes dans les données. Dans ce cas les données ne sont pas labellisées. Donc on a pas d’information préalable sur la sortie, on cherche surtout à comprendre la projection des données dans l’espace.

### Liens utiles


[Machine Learning Bases](https://lesdieuxducode.com/blog/2018/11/le-machine-learning-12)

[Machine Learning Code](https://lesdieuxducode.com/blog/2018/11/le-machine-learning-22---exemples-pratiques)

[Big Data & Machine Learning](https://www.lebigdata.fr/machine-learning-et-big-data)

Merci à vous.

