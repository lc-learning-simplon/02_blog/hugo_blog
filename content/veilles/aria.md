---
title: "ARIA"
date: 2019-07-23T16:40:19+02:00
draft: false
description: "Veille sur Accessible Rich Internet Applications."
img: ""
author: "Cathy"
---
## Description

Quand on cherche le mot Aria, il nous ramène vers une pièce de musique pour l’opéra, mais pour nous Aria peut se définir comme le personnage de Games of thrones, ou comme une commune d’Espagne. Sa peut être aussi un gestionnaire libre de téléchargement unix. 

Mais pour se qui nous concerne Aria est l’accessible Rich Internet Applications (ou Applications Internet riches accessibles)et c’est une spécification technique du W3C.

## Qu'est-ce-que-c'est?

Sa définit des moyens de créer du contenu et des applications web (en particulier celles qui sont développés à l’aide d’Ajax et de JavaScript) plus accessibles aux personnes handicapées. 
Par exemple, ARIA autorise des points de repère de navigation accessibles, des widgets JavaScript, des suggestions pour les formulaires et des messages d’erreurs,la mise à jour de contenu dynamique, et bien d'autres choses encore. La principale mission est de proposer des solutions techniques pour rendre le World Wide Web accessible aux personnes handicapées et d'une manière plus générale à toute personne sans nécessiter de pré-requis particulier.

## Trois Caractéristiques principales sont définies dans la spécification:

### Rôles:

    • Ceux-ci définissent ce qu'un élément fait. 
    Bon nombre de ces rôles sont des rôles de référence, 
    qui dupliquent en grande partie la valeur sémantique des éléments structurels HTML5, 
    par exemple role = "navigation" (<nav>) ou role="complementary" (<aside>),
    mais il en existe d'autres qui décrivent différentes structures de pages, 
    telles que role="banner", role="search", role="tabgroup", role="tab",
    etc., que l'on trouve couramment dans les UIs.

### Propriétés:

    • Ceux-ci définissent les propriétés des éléments, 
    qui peuvent être utilisés pour leur donner une signification supplémentaire ou une sémantique.
    Par exemple, aria-required="true" spécifie qu'une entrée de formulaire doit être renseignée pour être valide,
    alors que aria-labelledby="label" vous permet de mettre un ID sur un élément,
    puis de le référencer. en tant qu'étiquette pour tout autre élément de la page,
    y compris plusieurs éléments, ce qui n'est pas possible avec <label for="input">.
    A titre d'exemple, vous pouvez utiliser aria-labelledby  pour spécifier qu'une description de clé contenue dans un <div>
    est le label  de plusieurs cellules de tableau, ou vous pouvez l’utiliser comme alternative à l’image alt text —  
    spécifiez les informations existantes sur la page en tant que image alt text,
    plutôt que de devoir les répéter à l'intérieur de l'attribut alt .  

### États:

    • Propriétés spéciales qui définissent les conditions actuelles des éléments, 
    telles que aria-disabled="true", qui spécifient à un lecteur d'écran que l'entrée de formulaire est actuellement désactivée.
    Les états diffèrent des propriétés en ce que les propriétés ne changent pas tout au long du cycle de vie d'une application, 
    alors que les états peuvent changer, généralement par programmation via JavaScript.


Un point important sur les attributs est qu'ils n'affectent en rien la page Web, à l'exception des informations exposées par les API d'accessibilité du navigateur (où les lecteurs d'écran obtiennent leurs informations). ARIA n'affecte pas la structure de la page Web, le DOM, etc., bien que les attributs puissent être utiles pour sélectionner des éléments par CSS.

## L'objectif:

L'objectif est d'accroître l'accessibilité des contenus dynamiques et des composantes des interfaces dynamiques développées à l'aide d'Ajax, HTML, Javascript et technologies associées. Le HTML ne propose pas de fonctionnalité permettant de créer du contenu dynamique ni des interfaces de contrôle mais permet l'insertion d'applets (Flash, Java) et de scripts traités côté client (généralement Javascript). Les développeurs web utilisent de plus en plus le Javascript pour créer les interfaces de contrôle qu'ils ne peuvent créer à l'aide de HTML seul. Ils utilisent également ces scripts pour actualiser une partie de la page sans pour autant recharger l'ensemble de la page depuis le serveur web. Ces techniques sont dites des applications internet « riches ». Ces interfaces et ces sections actualisées ne sont souvent pas accessibles aux utilisateurs ayant des déficiences, notamment à ceux utilisant des lecteurs d'écran ou à ceux ne pouvant pas utiliser de souris ni d'équipement équivalent.
ARIA décrit comment ajouter de la sémantique et des métadonnées aux contenus HTML afin de rendre les contrôles d'interface et les contenus dynamiques plus accessibles. Par exemple, il devient possible d'identifier une liste de liens en tant que menu de navigation et d'indiquer si son état est plié ou déplié. Quoique conçu pour traiter de l'accessibilité en HTML, l'utilisation de ARIA n'est pas nécessairement restreinte au HTML mais peut être étendue à d'autres langages comme Scalable Vector Graphics (SVG).
ARIA permet aux pages Web (ou à des parties de pages) de se déclarer comme des applications plutôt que comme de simples documents statiques, par l'ajout de rôles, de propriétés ou d'états d'information vers des applications web dynamiques.
ARIA est destinée à être utilisée par les développeurs d'applications Web, les navigateurs web (ou agents utilisateurs), les technologies d'assistance (ou ATS), et les outils d'évaluation à l'accessibilité.

## Quelques Liens:

https://www.w3.org/WAI/standards-guidelines/aria/#top

https://fr.wikipedia.org/wiki/Accessible_Rich_Internet_Applications

https://developer.mozilla.org/fr/docs/Apprendre/a11y/WAI-ARIA_basics

https://www.w3.org/TR/html-aria/

Merci.