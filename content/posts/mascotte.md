---
title: "Venez découvrir la mascotte de notre promo"
date: 2019-07-21T11:15:07+02:00
draft: false
description: "Présentation de la mascotte."
img: ""
author: "Les !MPOSTEURS"
---


#### Après les parrains, la mascotte …

L’animal mignon choisi par nos formateurs pour représenter la promo… **le wombat** :) 

On va juste passer sous silence le fait que c’est un animal sacrificiel et plutôt dire que ce choix témoigne de la tendresse et de l’attachement que nos formateurs nous porte  !

*La presse enjolive toujours les faits donc pourquoi pas les blogs aussi…*

<img src= "/img/wombat/wombat.jpg" style: 40%;>

Outre le fait que je voulais agrémenter le blog “d’une photo mignonne de nos chers amis à quatre pattes” ma démarche était également motivée par un désir réel et noble *(lire faux mais nécessaire)* de revenir sur les fonctionnalités que nous offre la touche F12 *(légitimité du contenu oblige…)*.



#### Alors F12 (raccourci clavier) ou // click droit => examiner l’élément // ça sert à quoi ? 



Eh bien, j’ai interrogé notre cher ami Google et il se trouve que les réponses sont multiples…

Pas étonnant donc que nos formateurs se livrent à ce chantage émotionnel odieux... Oui, très chers lecteurs, vous avez bien lu... nos formateurs *nous menacent* et *répandent de fausses rumeurs à notre encontre* ! En effet, ils n'ont de cesse de dire à qui veut l'entendre que nous, pauvres apprenants, sommes en fait des meurtriers et que l'on tue froidement de pauvres betes innocentes. Je vous explique, pour eux, se balader sur le net et ne pas ouvrir le panel des développeurs équivaut à se rendre coupable de meurtre, de meurtre sur la personne adorable d’un wombat. Leur stratégie est évidente: nous faire culpabiliser afin de nous forcer à utiliser cet outil.

Bien que je ne puisse approuver leur méthode, *et croyez bien qu'il me peine de l’admettre* je comprends leur démarche. En effet, ce satané F12 s’avère fichtrement utile…

Ainsi, quand on consulte un site web, cliquer sur F12 nous ouvre une fenêtre dans un coin de l’écran  qui nous renseigne sur les différents éléments qui structurent la page.

Pratique quand on veut en construire une et que les blocs qu’on veut y disposer refusent de se comporter comme l'on voudrait. Les éditeurs de texte, si utiles soient-ils, ne permettent pas toujours une lecture explicite et dynamique du problème. Les marges qui “collapse” par exemple sont bien plus visibles si l’on se sert de l’onglet “inspecter l'élément”. 

Pratique également si vous voulez connaître la police utilisée pour rédiger du contenu ou la couleur spécifique utilisée pour mettre en avant un élément... Les feuilles de style sont disponibles dans leur intégralité et il nous est même permis de les modifier et de désactiver certaines règles appliquées.

Idem si vous êtes soucieux de garantir l'accessibilité à votre site au plus grand nombre...
Si vous voulez voir de quoi a l’air votre page web sur un média différent, ne cherchez pas plus loin... un simple click suffit 

C'est bien cela, F12 peut vous éviter d’avoir à utiliser votre portable, celui de votre petit neveu et du voisin du dessus ... Pour vous assurer que la mise en page soit conservée à travers les différents supports de lecture choisis par les utilisateurs de votre site, F12 vous propose ainsi d’afficher votre contenu sur plusieurs écrans correspondant aux modèles de tablettes et téléphones portables les plus répandus.

Les possibilités offertes par ce panel vont bien sur bien au delà de la modification du seul html et css... la console est, par exemple, très utile pour déboguer les scripts présents sur votre page. On y trouve aussi tout plein d’autres fonctionnalités mais je suis loin, très loin de les connaître toutes.

Je vais donc en rester là et vous inviter à éxécuter vous meme la manip... *n'oubliez pas la vie d'un wombat est en jeu...*


