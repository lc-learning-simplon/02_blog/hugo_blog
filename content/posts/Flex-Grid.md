---
title: "Flex & Grid"
date: 2019-07-30T11:15:07+02:00
draft: false
description: "Projets sites statiques présentant les deux rivaux Flex & Grid."
img: ""
author: "Les !MPOSTEURS "
---
## SEMAINE 5: FLEX/GRID 


Lors de cette semaine, nos formateurs ont concoctés de belles galères et arrachements de cheveux pour nous. 

But du projet: nous dégoûter de Flex & Grid afin d'en faire un site semi-fonctionnel présentant les deux grands rivaux.


Cet objectif fixé par les formateurs n'a pas porté tant que ça ses fruits... 

Au lieu de ne plus vouloir toucher à ceux-ci ce sont les problèmes de fusions Git et les soucis de cohérence d'équipes qui nous ont le plus posé problème


Conclusion que nous en tirons : 


- **Toujours pull !!**

- **Ne pas mélanger trop Grid & Flex**

- **Apprendre à communiquer et s'adapter aux autres** 


Enfin, cette semaine a fait grandir notre créativité, notre degré de remise en question mais nous a permis d'obtenir un résultat concret d'un premier "vrai" projet de groupe. Les tâches ont globalement été bien réparties, les designs étaient tous différents et originaux. 

La contrainte de rendre notre site accessible aux débutants a bien été respectée également.


## COMPTE RENDU DU 30 JUILLET 2019 


Le mardi de la semaine suivante, les présentations ont donc débuté en début de matinée.

Répétitions de <code>justify-content</code>, sur-utilisation du <code>grid-column</code> ! 


Mais aussi des sites plus ou moins fonctionnels # LOL #BIENVEILLANCE

Passant de sites avec canards qui explosent, de Netflix qui propose son interface programmation avec "Net-Flex" rempli d'easter eggs et de sites aux couleurs chatoyantes....

Vous en aurez pour tout les goûts ! 

La prés. s'est déroulé dans le respect du travail fourni par chacun et la rigolade, une ambiance que l'on retrouve à chaques veilles ou projets


**CHALLENGE DIFFICILE MAIS RIEN NE FAIS PEUR AUX !MPOSTEURS**

*OU PRESQUE....*
(**La suite au prochain épisode....**)

## LIENS VERS NOS SITES STATIQUES 

Projet Caroline - Thomas - Maxime : [Flex-Grid](https://gitlab.com/Maxime_DO/flex---grid)

Projet Cathy - Lucas - Abdellah : [Flex-Grid](https://gitlab.com/kitkat4631/flexgrid.git)

Projet Marea - Élodie - Ridhoine : [Flex-Grid](https://gitlab.com/RIDHOINE/flex-grid-groupe-5)

Projet Dimitri - Boris - Marie : [Flex-Grid](https://gitlab.com/DImitri31300/flex_grid_mbd)

Projet Cécile - Mylène - Emma : [Flex-Grid](https://gitlab.com/tuto-grid-flex/tuto-grid--flex)

### MERCI À TOUS DE VOTRE ATTENTION ! 