---
title: "Zoom Sur… Notre Promo Soeur La Promo Dév Web Et Mobile De Carbonne"
date: 2019-08-12T13:50:01+02:00
draft: false
description: "Présentation de la Promo Carbonne."
img: ""
author: "Les !MPOSTEURS"
---
<img src="/img/zoom/minion.jpg" alt="" style= "widht: 20%;">


Séparés par la distance mais toujours dans nos coeurs, petit clin d'oeil vers les *“cocos et les cocottes”* de Carbonne. 
Pour que ça soit *Claire*, il n’y aura pas de blagues de *blonde* mais juste de l’info plus ou moins utiles dans ce poste… Lecteurs non avertis sachez que ces personnes assurent comme des *chefs* … (et oui il n’y a pas qu’en cuisine qu’on trouve des chefs Mlle Echeveste. *Pouet!*) 
Oui, nous faisons tous partie de la même et grande famille qu’est Simplon…

Cela étant dit, techniquement nous sommes l'aîné (ben oui les gars 3 petits jours ça compte donc ,voilà, n’oubliez jamais que vous nous devez le respect!)
Plus sérieusement, on partage tout:

* nos cours
* nos prises de tête ( et je vous assure que ça fait déjà beaucoup)
* un channel Discord ...
* et même notre jouet et doudou favori “le Allan” donc si vous prenez plaisir à parcourir ce blog (ou pas d’ailleurs) n’hésitez pas à zieuter le site de leur promo…

En effet, si on pousse la métaphore un peu loin, ne dit on pas que les enfants, même s’ils sont parfois élevés de la même manière et s’ils bénéficient des mêmes conditions d’apprentissage, feront et deviendront des personnes totalement différentes.
Je pense que cet adage trouve également matière à s’appliquer dans le cas présent...

Soyez donc curieux et allez découvrir le site de notre promo jumelle: [https://promo-simplon-carbonne.gitlab.io/] Je suis sûre que vous y trouverez tout un tas d’infos utiles.

(Pour nous y etre rendu physiquement, on peut vous garantir que leur promo vaut le détour. Ils sont très accueillants et ne manquent pas de ressources donc n’hésitez pas!)

Finissons ce billet par une petite dédicace spéciale… Carbonne *(Timothée)* , celle-ci est pour toi! 

<img src="/img/zoom/terminator-comic.jpg" alt="" style= "width: 40%;">

