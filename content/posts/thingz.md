---
title: "Thingz"
date: 2019-07-17T15:29:50+02:00
draft: false
description: "Présentation des Parrains de la Promotion."
img: ""
author: "Les !MPOSTEURS"
---
<img src= "/img/accueil/thingz.png" alt="">

## NOS PARRAINS !

Nous avons l'immense privilège de découvrir la start up Thingz via le parrainage de notre promotion par les fondateurs de cette start up.
ils ont su allier pédagogie et ludicité afin d'apprendre les bases du codage aux jeunes enfants et même aux moins jeunes ;)


L'idée est simple mais ingénieuse, mélanger scrap qui est un logiciel pédagogique efficace à un petit boîtier où s'insérent des éléments permettant d'interagir avec scrap. le principe est basique: insérer des éléments type buzzer, leds, boutons poussoirs ou encore afficheur numérique (et plus si affinités) sur ce petit boitier connecté à un ordinateur afin de permettre aux utilisateurs d'expérimenter dans le réél le code qu'ils auront créé sur scrap. Ainsi le codage n'est plus abstrait mais bien concret et les utilisateurs peuvent immédiatement voir le résultat de leur codage


L'ingéniosité dans la simplicité, les enseignants ont coutume de dire on devient lecteur en lisant, et cet adage ils l'ont appliqué au codage, on devient codeur en codant


Le coup de génie vient de l'aspect didactique qui peut se retranscrire parfaitement à l'école. En effet, pour utiliser ce logiciel vous devrez passer par un scrap un peu spécial. Ce dernier est hébergé sur le site de THINGZ et ainsi vous avez la possibilité en tant qu'enseignant ou même en tant que parents, de créer des hubs pouvant réunir plusieurs élèves et élaborer ansi des exercices spécifiques pour vos élèves.


Nous avons vraiment été impressionnés par l'ingéniosité et la créativité de nos parrains et apprécions leur démarche pédagogique qui s'insère tout à fait dans l'esprit d'une formation telle que la nôtre.


Nous ne doutons pas de leur réussite et espérons avoir autant de créativité et d'enthousiasme pour notre future carrière.
Merci encore de nous avoir montré l'utilisation du codage et d'avoir permis de rendre son application moins abstraite.


 
Pour en savoir plus, on t'invite à aller visiter leurs sites :

[Thingz.co](https://thingz.co/)